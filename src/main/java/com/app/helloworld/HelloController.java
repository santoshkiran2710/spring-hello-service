package com.app.helloworld;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@RequestMapping(value="/", method= { RequestMethod.POST, RequestMethod.GET})
    public String sayHello() {
		return "Hello Spring boot again";
		
	} 
	
	@RequestMapping(value="/hello/{name}", method= { RequestMethod.POST, RequestMethod.GET})
    public String sayHelloName(@PathVariable("name") String name) {
		return "Hello "+name+" to Spring boot";
		
	} 
}
